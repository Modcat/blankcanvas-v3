const { execSync } = require('child_process')

// Only showing the relevant config for hooks, for brevity
module.exports = {
  hooks: {
    postPackage: async (forgeConfig, options) => {
      const { outputPaths, platform } = options
      const appDir = platform === 'darwin' ? '/blankcanvas.app/Contents/Resources/app': '/resources/app/'
      execSync(`cd ${ outputPaths[0] }${ appDir } && yarn install --production`)
      execSync(`cd ${ outputPaths[0] }${ appDir }/src/blankcanvas-app-backend && yarn`)
      execSync(`cd ${ outputPaths[0] }${ appDir }/src/blankcanvas-app-frontend && yarn`)
    }
  },
  packagerConfig: {},
  makers: [
    {
      name: "@electron-forge/maker-squirrel",
      config: { name: "blankcanvas" }
    },
    {
      name: "@electron-forge/maker-zip",
      platforms: [ "darwin" ]
    },
    {
      name: "@electron-forge/maker-deb",
      "config": {}
    },
    // {
    //   name: "@electron-forge/maker-rpm",
    //   "config": {}
    // }
  ]
}