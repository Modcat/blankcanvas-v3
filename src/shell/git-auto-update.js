const path = require("path");
const { exec } = require("child_process");
const cwd = path.join(__dirname, "");
const { Notification } = require("electron");

module.exports = async () => {
  const updateScripts = {
    app() {
      exec("cd ../../ && git pull && yarn", { shell: true, cwd });
    },
    frontend() {
      exec("cd ../blankcanvas-app-frontend && git pull && yarn ", {
        shell: true,
        cwd,
      });
    },
    backend() {
      exec("cd ../blankcanvas-app-backend && git pull && yarn", {
        shell: true,
        cwd,
      });
    },
  };

  updateScripts.app();
  updateScripts.frontend();
  updateScripts.backend();

  // Feathers socket connection to do live updates when recieved from the Gitlab CI -> Conceivable Media DSL -> here...

  const feathers = require("@feathersjs/feathers");
  const socketio = require("@feathersjs/socketio-client");
  const io = require("socket.io-client");

  const socket = io("https://conceivable-media.com/");
  const app = feathers();
  app.configure(socketio(socket));

  app.service("send-tag-release").on("created", (updateInfo) => {
    if (updateInfo.repo === "blankcanvas-app") {
      updateScripts.app();
      //   TODO notifiy the user the main app needs restarting
    }

    if (updateInfo.repo === "blankcanvas-app-frontend") {
      updateScripts.frontend();
    }

    if (updateInfo.repo === "blankcanvas-app-backend") {
      updateScripts.backend();
    }

    new Notification({
      title: `Upading ${updateInfo.repo} / ${updateInfo.branch}`,
      body: `Hashcode: ${updateInfo.hash.substr(0, 12)}`,
    }).show();
  });
};
