// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
const pathCMD = spawn('yarn frontend', { shell: true, cwd });

pathCMD.stdout.on('data', (data) => {
    win.webContents.send('asynchronous-reply', `@@PATH:stdout@@ ${data}`)
});