const path = require('path');
const { spawn } = require('child_process')
const cwd = path.join(__dirname, '').split('src')[0]
const kill = require('kill-port')

module.exports = async(win) => {

    await kill(3030,'tcp')
    
    const backend = spawn('yarn backend', { shell: true, cwd });

    backend.stdout.on('data', (data) => {
        win.webContents.send('logs', `@@backend:stdout@@ ${data}`)
    });

    backend.stderr.on('data', (data) => {
        win.webContents.send('logs', `@@backend:stderr@@ ${data}`)
    });

    backend.on('close', (code) => {
        win.webContents.send('logs', `@@backend:stderr@@ child process exited with code ${code}`)
    });
}