const path = require('path');

module.exports = function() {
    import('fix-path').then(e => e.default());
    
    const cwd = path.join(__dirname, '').split('src')[0],
    delimiter = process.platform === 'win32' ? ';' : ':'
    
    process.env.PATH += `${delimiter}${cwd}node_modules/.bin`
}