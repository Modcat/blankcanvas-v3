const { app, BrowserWindow } = require('electron')
const path = require('path')
require('./shell/env-set')()
const kill = require('kill-port')

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

let win
const createWindow = () => {
  const mainWindow = new BrowserWindow({
    width: 620,
    height: 380,
    frame: true,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  });
  win = mainWindow
  mainWindow.loadFile(path.join(__dirname, 'index.html'));
  mainWindow.webContents.openDevTools();
};
app.on('ready', createWindow);

app.on('window-all-closed', async() => {
  await kill(50456, 'tcp')
  await kill(3030, 'tcp')
  app.quit();
});

app.on('before-quit', async() => {
  await kill(50456, 'tcp')
  await kill(3030, 'tcp')
  app.quit();
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

app.on('ready', () => {
  
  win.webContents.on('did-finish-load', () => {
    
    require('./shell/git-auto-update')()

    require('./shell/frontend')(win)

    require('./shell/backend')(win)

    require('./shell/ipc-main')
  });
})