const setupFeathers = function () {
  const feathers = require('@feathersjs/feathers');
  const socketio = require('@feathersjs/socketio-client');
  const io = require('socket.io-client');

  const socket = io('http://api.feathersjs.com');
  const app = feathers();

  // Set up Socket.io client with the socket
  app.configure(socketio(socket));

  // Receive real-time events through Socket.io
  app.service('messages')
    .on('created', (message) => console.log('New message created', message));

  // Call the `messages` service
  app.service('messages').create({
    text: 'A message from a REST client',
  });

  app.io.on('connect', (reason) => {
    // Show offline message
  });

  app.io.on('disconnect', (reason) => {
    // Show offline message
  });
};

export { setupFeathers };
