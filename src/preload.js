const { contextBridge } = require('electron')
const { ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('mainThread', {
  Electron: true,
  send(channel, data) {
    ipcRenderer.send(channel, data);
  },
  recieve(channel, callback) {
    ipcRenderer.on(channel, callback)
  }
})

